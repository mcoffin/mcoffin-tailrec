//! Crate for implementing tail-call recursion patterns in constant stack space in Rust.
//!
//! `tailrec` is the most powerful of the implmentations, but all should acheive the exact same
//! compilation results if used properly.
//!
//! See `examples/strip_prefix.rs` for an example usage of all four implementations, that will
//! compile down to the same implementation (at least in --release mode for cargo)

/// Enum returned by tail-call recursive functions.
///
/// `Continue` indicates that the tail-recursive function should be invoked again.
///
/// `Done` indicates that the tail-recursive function is done executing, and the contained value
/// should be returned.
///
/// See: `tailrec`
#[derive(Debug, Clone, Copy)]
pub enum TailrecResult<State, Ret> {
    Continue(State),
    Done(Ret),
}

impl<State, Ret> TailrecResult<State, Ret> {
    /// Returns `true` if this is a `Continue(..)` value, `false` otherwise.
    #[inline(always)]
    pub fn is_continue(&self) -> bool {
        match self {
            &TailrecResult::Continue(..) => true,
            _ => false,
        }
    }

    /// Returns `true` if this is a `Done(..)` value, `false` otherwise.
    #[inline(always)]
    pub fn is_done(&self) -> bool {
        match self {
            &TailrecResult::Done(..) => true,
            _ => false,
        }
    }
}

/// Tail-call recursion implementation using an enum, and an initial state
///
/// `f` should return `TailrecResult::Continue(..)` if it wants to be invoked again, and
/// `TailrecResult::Done(..)` if it has finished executing
///
#[inline]
pub fn tailrec<F, State, Ret>(mut state: State, mut f: F) -> Ret where
    F: FnMut(State) -> TailrecResult<State, Ret>,
{
    use TailrecResult::*;
    loop {
        match f(state) {
            Continue(new_state) => {
                state = new_state;
            },
            Done(ret) => {
                return ret;
            },
        }
    }
}

/// Tail-call recursion implementation using a mutable reference to the `State`
///
/// `f` should return `Some(..)` to indicate it is done executing, or `None` to indicate it should
/// be invoked again
///
/// See: `tailrec_ref_owned`
#[inline]
pub fn tailrec_ref<F, State, Ret>(state: &mut State, mut f: F) -> Ret where
    F: FnMut(&mut State) -> Option<Ret>,
{
    loop {
        if let Some(v) = f(state) {
            return v;
        }
    }
}

/// Wrapper for `tailrec_ref` that takes ownership of `state`.
///
/// These are useful in cases where `f` doesn't successfully inline, and `State` is big enough that
/// you don't want to be copying it around at all to pass back in to `f`.
///
/// The below example is a stupid implementation, but illustrates the point.
///
/// ```rust
/// fn count_leading<T: Sized + Eq + Copy>(buf: [T; 4096], v: T) -> usize {
///     use mcoffin_tailrec::tailrec_ref_owned;
///     let state = (buf, 0usize, 0usize);
///     tailrec_ref_owned(state, |&mut (ref buf, ref mut idx, ref mut count)| {
///         let ret = if (buf[*idx] == v) {
///             *count += 1;
///             None
///         } else {
///             Some(*count)
///         };
///         *idx += 1;
///         ret
///     })
/// }
/// let mut buf: [u8; 4096] = [0; 4096];
/// buf[4095] = 1;
/// assert_eq!(count_leading(buf, 0), 4095);
/// ```
#[inline(always)]
pub fn tailrec_ref_owned<F, State, Ret>(mut state: State, f: F) -> Ret where
    F: FnMut(&mut State) -> Option<Ret>,
{
    tailrec_ref(&mut state, f)
}

/// Convenience version of `tailrec` for when `State` and `Ret` would be the same type, and `State`
/// implements the `Copy` trait.
///
/// Favors use of `Option<_>` instead of `TailrecResult<_, _>` for this case.
///
/// # Example
///
/// ```rust
/// fn strip_leading_whitespace<'a>(s: &'a str) -> &'a str {
///     use mcoffin_tailrec::tailrec_translate;
///     tailrec_translate(s, |s| s.strip_prefix(char::is_whitespace))
/// }
/// let result = strip_leading_whitespace("\t\t \t  \tHello, world!");
/// assert_eq!(result, "Hello, world!");
/// ```
#[inline]
pub fn tailrec_translate<F, State>(state: State, mut f: F) -> State where
    F: FnMut(State) -> Option<State>,
    State: Copy,
{
    tailrec_translate_ref(state, |&s| f(s))
}


/// Version of `tailrec_translate` that doesn't require the implementation of `Copy` by `State`.
/// This is acheived by passing state in to `f` by reference, to prevent an otherwise-necessary
/// copy.
///
/// You can use this to implement `tailrec_translate`-style functions when `State` implements
/// `Clone` but not `Copy`, but we left out a short-hand for it so that it's obvious that you will
/// be cloning your State.
///
/// The following example is really stupid, but it illustrates the concept
///
/// ```rust
/// use mcoffin_tailrec::tailrec_translate_ref;
/// let result = tailrec_translate_ref("".to_string(), |s| {
///     if s.len() >= 10 {
///         None
///     } else {
///         Some(format!("{}a", s))
///     }
/// });
/// assert_eq!(&result, "aaaaaaaaaa");
/// ```
#[inline]
pub fn tailrec_translate_ref<F, State>(mut state: State, mut f: F) -> State where
    F: FnMut(&State) -> Option<State>,
{
    loop {
        match f(&state) {
            Some(next) => {
                state = next;
            },
            None => break,
        }
    }
    state
}

#[cfg(test)]
mod test {
    //! The `strip_whitespace*` functions are `#[no_mangle]` for easy inspection of the test binary
    //! to ensure that they compile down to the same implementation

    #[inline(never)]
    #[no_mangle]
    fn strip_whitespace<'a>(orig: &'a str) -> &'a str {
        use super::{
            tailrec,
            TailrecResult,
        };
        tailrec(orig, |orig: &'a str| {
            orig.strip_prefix(|c| c == '\t' || c == ' ')
                .map(TailrecResult::Continue)
                .unwrap_or_else(|| TailrecResult::Done(orig))
        })
    }

    fn test_strip_whitespace(strip_fn: fn(&str) -> &str) {
        let s = "\t\t \t \tHello world!";
        assert_eq!(strip_fn(s), "Hello world!");
    }

    #[test]
    fn strip_whitespace_works() {
        test_strip_whitespace(strip_whitespace);
    }

    #[inline(never)]
    #[no_mangle]
    fn strip_whitespace_ref<'a>(mut orig: &'a str) -> &'a str {
        use super::tailrec_ref;
        tailrec_ref(&mut orig, |orig: &mut &'a str| {
            match orig.strip_prefix(|c| c == '\t' || c == ' ') {
                Some(s) => {
                    *orig = s;
                    None
                },
                None => Some(*orig),
            }
        })
    }

    #[test]
    fn strip_whitespace_ref_works() {
        test_strip_whitespace(strip_whitespace_ref);
    }
}
