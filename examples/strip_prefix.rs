//! You can run the following to ensure that all four of the following implementations of
//! `strip_whitespace` compile to exactly the same code.
//!
//! ```bash
//! cargo build --release --example strip_prefix
//! objdump -Ct target/release/examples/strip_prefix | grep -F '.text' | grep -F 'strip_whitespace'
//! ```
//! That should result in something similar to the following
//!
//! ```
//! 0000000000005350 g     F .text  000000000000023c              strip_whitespace_ref
//! 0000000000005350 g     F .text  000000000000023c              strip_whitespace
//! 0000000000005350 g     F .text  000000000000023c              strip_whitespace_translate
//! 0000000000005350 g     F .text  000000000000023c              strip_whitespace_loop
//! ```
//!
//! This should show that all three symbols resolve to the same address in the binary, meaning that
//! there was a link-time optimization to merge them since they were the same.

extern crate mcoffin_tailrec;

use std::{
    env,
};

#[inline(never)]
#[no_mangle]
fn strip_whitespace<'a>(orig: &'a str) -> &'a str {
    use mcoffin_tailrec::TailrecResult::*;
    mcoffin_tailrec::tailrec(orig, |orig: &'a str| {
        orig.strip_prefix(char::is_whitespace)
            .map(Continue)
            .unwrap_or_else(|| Done(orig))
    })
}

#[inline(never)]
#[no_mangle]
fn strip_whitespace_loop<'a>(mut orig: &'a str) -> &'a str {
    while let Some(s) = orig.strip_prefix(char::is_whitespace) {
        orig = s;
    }
    orig
}

#[inline(never)]
#[no_mangle]
fn strip_whitespace_ref<'a>(orig: &'a str) -> &'a str {
    use mcoffin_tailrec::tailrec_ref_owned;
    tailrec_ref_owned(orig, |orig: &mut &'a str| {
        match orig.strip_prefix(char::is_whitespace) {
            Some(s) => {
                *orig = s;
                None
            },
            None => Some(*orig),
        }
    })
}

#[inline(never)]
#[no_mangle]
fn strip_whitespace_translate<'a>(orig: &'a str) -> &'a str {
    use mcoffin_tailrec::tailrec_translate;
    tailrec_translate(orig, |s| s.strip_prefix(char::is_whitespace))
}

fn main() {
    for arg in env::args() {
        println!("original: {:?}", &arg);
        println!("stripped (tailrec): {:?}", strip_whitespace(&arg));
        println!("stripped (tailrec_ref): {:?}", strip_whitespace_ref(&arg));
        println!("stripped (tailrec_translate): {:?}", strip_whitespace_translate(&arg));
        println!("stripped (traditional loop): {:?}", strip_whitespace_loop(&arg));
    }
}
